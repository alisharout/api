package co.pragra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PragraApplication {

    public static void main(String[] args) {
        SpringApplication.run(PragraApplication.class, args);
    }

}
