package co.pragra.controller;

import co.pragra.model.Note;
import co.pragra.service.ScrapBookservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import java.util.Arrays;
import java.util.List;

@Controller
public class ScrapBookController {
    @Autowired
    private ScrapBookservice service;


    @RequestMapping("/getNotes")
    public String getAllNotes(Model model) {
        model.addAttribute("note",new Note());
        return "scrapbook";
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public String saveNote(Note note, Model model){
        List<Note> eachNote = service.saveNotes(note);
        model.addAttribute("note",eachNote);
        return "display";
    }
}
